﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BaiThi_EAP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        DataContextDataContext data = new DataContextDataContext();

        public bool Add(Employee value)
        {
            try
            {
                data.Employees.InsertOnSubmit(value);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;

            }
        }

        public List<Employee> searchEmployee(string departmentName)
        {
            try
            {
                List<Employee> emp = (from Employee in data.Employees where Employee.Departmentect == departmentName select Employee).ToList();
                return emp;
            }
            catch
            {
                return null;
            }
        }
    }
}